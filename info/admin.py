from django.contrib import admin
from .models import People
from django.db.models import QuerySet


class PeopleAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'position', 'salary', 'settle_up', 'parent')
    list_filter = ('position', 'parent')
    actions = ['del_settle_up']

    @admin.action(description='Delete selected settle up')
    def del_settle_up(self, request, qs: QuerySet):
        count_updated = qs.update(settle_up=People.empty)
        self.message_user(
            request,
            f'Was cleared {count_updated} records'
        )


admin.site.register(People, PeopleAdmin)
