from django.shortcuts import render
from .models import People


def show_peoples(request):
    peoples = People.objects.all()
    return render(request, 'list.html', {'peoples': peoples})
