# from .models import People

import random

FullName = "Jay Sanders", "Jim Marshall", "Roy Hill", "Axel Olson", "Billy Perry", "Charlie Gray", \
           "Jax Mills", "Gina Dunn", "Paul Thomas", "Ringo Olson", "Ally Perry", "Nicky Mills", "Cam Gray", \
           "Ari Sanders", "Trudie Marshall", "Cal Thomas", "Carl Hill", "Lady Castro", "Lauren Dunn", \
           "Ichabod Marshall", "Arthur Hill", "Ashley Olson", "Drake Perry", "Kim Gray", "Julio Mills", \
           "Lorraine Castro", "Floyd Castro", "Janet Thomas", "Lydia Perry", "Charles Dunn", "Pedro Sanders", \
           "Bradley Dunn", "Aaron Castro", "Abraham Hill", "Adam Gray", "Adrian Olson", "Aidan Marshall",\
            "Alan Cook", "Albert Cook", "Alejandro Marshall", "Alex Olson", "Alexander Hill", "Alfred Sanders", \
           "Andrew Mills", "Angel Thomas", "Anthony Perry", "Antonio Cook", "Ashton Rice", "Austin Perry", \
           "Daniel Sanders", "David Hill", "Dennis Marshall", "Devin Olson", "Diego Dunn", "Dominic Rice", \
           "Donald Thomas", "Douglas Rogers", "Dylan Gray", "Harold Mills", "Harry Rogers", "Hayden Marshall", \
           "Henry Dunn", "Herbert Cook", "Horace Sanders", "Howard Thomas", "Hugh Hill", "Hunter Sanders", \
           "Malcolm Perry", "Martin Marshall", "Mason Olson", "Matthew Gray", "Michael Dunn", "Miguel Rogers",\
            "Miles Marshall", "Morgan Hill"

Salary = "4200", "4300", "4450", "4550", "4250", "4150", "4600", "4700", "6900", "6850", "7250", "7450", "7350", \
         "7150", "6800", "9500", "9700", "9200"

Day = "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", \
      "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"

Month = "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"

Year = "2018", "2019", "2020", "2021"


def run_random():
    form = [0]
    count_one = 0
    for i_one in range(5):
        count_one = count_one + 1
        name = random.choice(FullName)
        people_salary = random.choice(Salary)
        people_year = random.choice(Year)
        people_month = random.choice(Month)
        people_day = random.choice(Day)
        form.append({'full_name': name, 'employment_date': people_year + '-' + people_month + '-' + people_day,
                     'salary': people_salary})
        # form = People(data={'full_name': name,
        # 'employment_date': people_year + '-' + people_month + '-' + people_day,
        #                     'salary': people_salary})
        # form.save()
    return form


run_random()
