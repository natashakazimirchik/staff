from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class People(MPTTModel):
    empty = 0
    full_name = models.CharField(max_length=80, unique=True)
    position = models.CharField(max_length=80)
    employment_date = models.DateField(max_length=80, auto_now_add=False)
    salary = models.DecimalField(max_digits=50, decimal_places=2)
    settle_up = models.DecimalField(max_digits=1000, decimal_places=2, null=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return self.full_name

    class MPTTMeta:
        order_insertion_by = ['full_name']
